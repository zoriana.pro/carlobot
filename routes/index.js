const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.send('Carlo cahtbot says hi');
});

router.get('/events', (req, res) => {
  res.json({
      events: [
          {
              type: 'music',
              name: 'Rammstein',
              genre: 'Heavy Metal',
              location: 'Munich',
              date: '11-10-2018'
          },
          {
              type: 'theatre',
              name: 'Les Miserables',
              location: 'Munich',
              date: '12-10-2018'
          },
      ]
  });
});

router.post('/dialogflow', (req, res) => {

});

module.exports = router;
